package com.nocola.showandroid.model.repository

import com.nocola.showandroid.model.db.PointDao
import com.nocola.showandroid.model.db.PointDataBaseModel
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.kotlin.verify

class RoomCacheDaoTest {
    private lateinit var sut: PointsDataBaseCacheDao
    private lateinit var dao: PointDao
    private val dbModels = listOf(
        PointDataBaseModel(0,1f, 1f),
        PointDataBaseModel(0,2f, 2f),
        PointDataBaseModel(0,3f, 3f),
    )

    private val points = listOf(
        Point(1f, 1f),
        Point(2f, 2f),
        Point(3f, 3f),
    )

    @Before
    fun setup() {
        dao = mock(PointDao::class.java)
        sut = PointsDataBaseCacheDao(dao)

        `when`(dao.getAllPoints()).thenReturn(Single.just(dbModels))
    }

    @Test
    fun whenGetGraph_dataRetrievedFromRoomDao() {
        // When
        sut.getPoints().blockingGet()
        // Then
        verify(dao).getAllPoints()
    }

    @Test
    fun whenGetGraph_daoReturnPointModels() {
        // When
        val res = sut.getPoints().blockingGet()
        // Then
        assertEquals(res, points)
    }

    @Test
    fun whenSave_oldDataClearedAndNewInserted() {
        // When
        sut.save(points)
        // Then
        verify(dao).deleteAll()
        verify(dao).insertAll(dbModels)
    }
}