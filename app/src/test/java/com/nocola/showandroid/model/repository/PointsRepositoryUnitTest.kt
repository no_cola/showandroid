package com.nocola.showandroid.model.repository

import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.kotlin.verify

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class PointsRepositoryUnitTest {
    private val amountPoints = 3
    private lateinit var sut: PointsRepository
    private lateinit var api: PointsApi
    private lateinit var dao: IPointsDao
    private lateinit var pointsCacheDao: IPointsCacheDao
    private val result = listOf(
        Point(1f, 1f),
        Point(2f, 2f),
        Point(3f, 3f),
    )


    @Before
    fun setup() {
        dao = mock(IPointsDao::class.java)
        api = mock(PointsApi::class.java)
        pointsCacheDao = mock(IPointsCacheDao::class.java)
        sut = PointsRepository(dao, pointsCacheDao)

        `when`(dao.load(amountPoints)).thenReturn(Single.just(result))
    }

    @Test
    fun whenLoad_daoUsedAsSource_cacheUsedToSaveData() {
        // When
        val res = sut.load(amountPoints).blockingGet()
        // Then
        verify(dao).load(amountPoints)
        verify(pointsCacheDao).save(res)
    }

    @Test
    fun whenLoad_resultFromDao() {
        // When
        val res = sut.load(amountPoints).blockingGet()
        // Then
        assertEquals(res, result)
    }

    @Test
    fun whenGetGraph_cacheRepoUsed() {
        // When
        sut.getGraph()
        // Then
        verify(pointsCacheDao).getPoints()
    }
}