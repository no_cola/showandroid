package com.nocola.showandroid.model.repository

import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.kotlin.verify

class NetworkGraphDaoTest {
    private lateinit var sut: NetworkPointsDao
    private lateinit var api: PointsApi
    private val response = PointsResponse(
        listOf(
            Point(1f, 1f),
            Point(2f, 2f),
            Point(3f, 3f),
        )
    )
    private val amount: Int get() = response.points.size

    @Before
    fun setup() {
        api = mock(PointsApi::class.java)
        sut = NetworkPointsDao(api)

        `when`(api.getPoints(amount)).thenReturn(Single.just(response))
    }

    @Test
    fun whenLoad_apiUsedAsSource() {
        // When
        sut.load(amount)
        // Then
        verify(api).getPoints(amount)
    }

    @Test
    fun whenLoad_dataFromApiIsUsed() {
        // When
        val res = sut.load(amount).blockingGet()
        // Then
        assertEquals(res, response.points)
    }
}