package com.nocola.showandroid.domain

import com.nocola.showandroid.model.repository.IPointsRepository
import com.nocola.showandroid.model.repository.Point
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.kotlin.verify

class LoadGraphUseCaseTest {
    private lateinit var sut: LoadPointsUseCase
    private lateinit var repo: IPointsRepository
    private val points = listOf(
        Point(1f, 1f),
        Point(2f, 2f),
        Point(3f, 3f),
    )
    private val amount: Int get() = points.size

    @Before
    fun setup() {
        repo = mock(IPointsRepository::class.java)
        sut = LoadPointsUseCase(repo)

        `when`(repo.load(amount)).thenReturn(Single.just(points))
    }

    @Test
    fun whenLoadGraph_repositoryIsUsed() {
        // When
        sut.loadPoints(amount).blockingGet()
        // Then
        verify(repo).load(amount)
    }

    @Test
    fun whenLoadGraph_resultComesFromRepo() {
        // When
        val result = sut.loadPoints(amount).blockingGet()
        // Then
        assertEquals(result, points)
    }
}