package com.nocola.showandroid.ui.graph.view

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import androidx.recyclerview.widget.RecyclerView

/**
 * Decorates view as cell in table.
 */
class CellDecorator : RecyclerView.ItemDecoration() {
    private val paint: Paint = Paint().apply {
        isAntiAlias = true
        color = Color.DKGRAY
        style = Paint.Style.STROKE
    }

    override fun onDrawOver(
        canvas: Canvas,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        paint.strokeWidth = parent.context.resources.displayMetrics.density

        val xStart = 0F
        val xEnd = parent.width.toFloat()

        for (i in 0 until parent.childCount) {
            val child = parent.getChildAt(i)
            val params = child.layoutParams as RecyclerView.LayoutParams
            val yTop = child.bottom.toFloat()
            val yBottom = params.height.toFloat()
            canvas.drawLine(xStart, yTop, xEnd, yTop, paint)
            val xCenter = xEnd / 2F
            canvas.drawLine(xCenter, yTop, xCenter, yBottom, paint)
        }
    }
}