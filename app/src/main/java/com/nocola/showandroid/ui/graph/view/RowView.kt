package com.nocola.showandroid.ui.graph.view

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat

class CellModel(val data: String)

/**
 * Represents row in table.
 */
class RowView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayoutCompat(context, attrs, defStyleAttr) {
    private var cells: Array<TextView> = emptyArray()
    var model: Array<CellModel>? = null
        set(value) {
            field = value
            updateView(value)
        }

    init {
        orientation = HORIZONTAL
        layoutParams = ViewGroup.LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.WRAP_CONTENT
        )
        gravity = Gravity.CENTER
    }

    private fun updateView(data: Array<CellModel>?) {
        if (data == null) {
            cells.forEach { it.text = null }
        } else {
            if (cells.isEmpty())
                createCells(data)

            cells.withIndex().forEach {
                it.value.text = data[it.index].data
            }
        }
    }

    private fun createCells(data: Array<CellModel>) {
        cells = Array(data.size) {
            val tv = TextView(context).apply {
                layoutParams = LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                ).apply {
                    weight = 1F
                }
                gravity = Gravity.CENTER
            }
            addView(tv)
            tv
        }
    }
}