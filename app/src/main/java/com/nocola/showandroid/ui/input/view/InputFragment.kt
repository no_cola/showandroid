package com.nocola.showandroid.ui.input.view

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.jakewharton.rxbinding3.view.clicks
import com.nocola.showandroid.R
import com.nocola.showandroid.common.BaseFragment
import com.nocola.showandroid.common.EventObserver
import com.nocola.showandroid.common.color
import com.nocola.showandroid.common.defaultAnimations
import com.nocola.showandroid.databinding.FragmentInputBinding
import com.nocola.showandroid.di.IDaggerApp
import com.nocola.showandroid.di.ViewModelFactory
import com.nocola.showandroid.ui.input.viewModel.InputViewModel
import com.nocola.showandroid.ui.input.viewModel.State
import io.reactivex.rxkotlin.addTo
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val WINDOW_DURATION = 1L

class InputFragment : BaseFragment() {
    private var binding: FragmentInputBinding? = null

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private val viewModel: InputViewModel by viewModels { viewModelFactory }

    private fun showMessage(msg: String) {
        Toast.makeText(requireContext(), msg, Toast.LENGTH_LONG).show()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as IDaggerApp).inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentInputBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            inputAmount.addTextChangedListener { editable ->
                editable?.let {
                    viewModel.amount =
                        if (it.isEmpty()) 0 else it.toString().toInt()
                }
            }
            tilInputAmount.hint = getString(R.string.input_hint, viewModel.maxAmount)
            btnStart.clicks()
                .throttleFirst(WINDOW_DURATION, TimeUnit.SECONDS)
                .subscribe {
                    viewModel.load()
                }.addTo(disposable)

            viewModel.uiState.observe(viewLifecycleOwner, EventObserver {
                checkButton(it)
                checkInput(it)
                if (it is State.Loaded)
                    showGraph()
                if (it is State.Error)
                    showMessage(it.message)

            })
        }

    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }
    private fun checkButton(state: State) {
        binding?.btnStart?.isEnabled = state == State.Ready || state == State.Loaded
    }

    private fun checkInput(state: State) {
        binding?.inputAmount?.setTextColor(
            if (state is State.Wrong) {
                Color.RED
            }
            else color(requireActivity(), R.attr.colorPrimary)
        )
    }

    private fun showGraph() {
        findNavController().navigate(
            R.id.graphFragment,
            null,
            defaultAnimations()
        )
    }
}