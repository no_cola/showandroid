package com.nocola.showandroid.ui.graph.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.os.Parcelable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.MotionEvent.INVALID_POINTER_ID
import android.view.ScaleGestureDetector
import android.view.View
import com.nocola.showandroid.R
import com.nocola.showandroid.model.repository.Point
import kotlinx.parcelize.Parcelize

private const val DEFAULT_SCALE = 1.5F

@Parcelize
private class State(
    val scale: Float,
    val shiftX: Float,
    val shiftY: Float,
    val superState: Parcelable?
) : Parcelable

class GraphView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr),
    ScaleGestureDetector.OnScaleGestureListener {

    private val scaleDetector = ScaleGestureDetector(context, this)
    private var density = 1F
    private var shiftX = 0F
    private var shiftY = 0F
    private var activePointerId = INVALID_POINTER_ID
    private var lastTouchX = 0F
    private var lastTouchY = 0F
    private var dotRadius: Float

    private val graphPaint = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.STROKE
    }

    private val dotPaint = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.STROKE
    }

    private val path = Path()

    private var scaleFactor = DEFAULT_SCALE
        set(value) {
            field = value
            postInvalidate()
        }

    var data: Array<Point>? = null
        set(value) {
            field = value
            postInvalidate()
        }

    init {
        density = context.resources.displayMetrics.density
        val typedValue =
            context.obtainStyledAttributes(attrs, R.styleable.GraphView)
        graphPaint.strokeWidth =
            typedValue.getDimension(R.styleable.GraphView_lineWidth, density)
        graphPaint.color =
            typedValue.getColor(R.styleable.GraphView_lineColor, Color.BLACK)
        dotPaint.color =
            typedValue.getColor(R.styleable.GraphView_dotColor, Color.BLACK)
        dotRadius =
            typedValue.getDimension(R.styleable.GraphView_dotRadius, density)
        val dotIsFilled =
            typedValue.getBoolean(R.styleable.GraphView_dotFilled, true)
        dotPaint.style =
            if (dotIsFilled) Paint.Style.FILL else Paint.Style.STROKE
        typedValue.recycle()

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(
            MeasureSpec.getSize(widthMeasureSpec),
            MeasureSpec.getSize(heightMeasureSpec)
        )
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            // Move center (x: 0, y: 0) to center of the screen
            canvas.translate(width / 2F, height / 2F)

            // Draw points
            path.reset()
            val turnY = -1

            data?.let { points ->
                var point = points.first()
                var cx = point.x * density * scaleFactor + shiftX
                var cy = point.y * density * turnY * scaleFactor + shiftY

                path.moveTo(cx, cy)
                canvas.drawCircle(cx, cy, dotRadius, dotPaint)

                for (i in 1 until points.size) {
                    point = points[i]
                    cx = point.x * density * scaleFactor + shiftX
                    cy = point.y * density * turnY * scaleFactor + shiftY
                    path.lineTo(cx, cy)
                }
                canvas.drawPath(path, graphPaint)


                for (i in 1 until points.size) {
                    point = points[i]
                    cx = point.x * density * scaleFactor + shiftX
                    cy = point.y * density * turnY * scaleFactor + shiftY
                    canvas.drawCircle(cx, cy, dotRadius, dotPaint)
                }

            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        scaleDetector.onTouchEvent(event)

        event?.let { ev ->
            when (ev.action) {
                MotionEvent.ACTION_DOWN -> {
                    ev.action.also { pointerIndex ->
                        lastTouchX = ev.getX(pointerIndex)
                        lastTouchY = ev.getY(pointerIndex)
                    }
                    activePointerId = ev.getPointerId(0)
                }

                MotionEvent.ACTION_MOVE -> {
                    val (x: Float, y: Float) =
                        ev.findPointerIndex(activePointerId)
                            .let { pointerIndex ->
                                ev.getX(pointerIndex) to
                                        ev.getY(pointerIndex)
                            }

                    shiftX += x - lastTouchX
                    shiftY += y - lastTouchY

                    invalidate()

                    lastTouchX = x
                    lastTouchY = y
                }
                MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                    activePointerId = INVALID_POINTER_ID
                }
                MotionEvent.ACTION_POINTER_UP -> {

                    ev.action.also { pointerIndex ->
                        ev.getPointerId(pointerIndex)
                            .takeIf { it == activePointerId }
                            ?.run {
                                val newPointerIndex =
                                    if (pointerIndex == 0) 1 else 0
                                lastTouchX =
                                    ev.getX(newPointerIndex)
                                lastTouchY =
                                    ev.getY(newPointerIndex)
                                activePointerId = ev.getPointerId(newPointerIndex)
                            }
                    }
                }
                else -> {
                    // do nothing
                }
            }
        }

        return true

    }

    override fun onSaveInstanceState(): Parcelable {
        // Save current state (scale and position)
        return State(scaleFactor, shiftX, shiftY, super.onSaveInstanceState())
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        // Restore previous state (scale and position)
        var s = state
        (s as? State)?.let {
            scaleFactor = it.scale
            shiftX = it.shiftX
            shiftY = it.shiftY
            s = it.superState
        }

        super.onRestoreInstanceState(s)
    }

    override fun onScale(detector: ScaleGestureDetector?): Boolean {
        scaleFactor *= detector?.scaleFactor ?: DEFAULT_SCALE
        return true
    }

    override fun onScaleBegin(detector: ScaleGestureDetector?): Boolean {
        return true
    }

    override fun onScaleEnd(detector: ScaleGestureDetector?) {
        // pass
    }

}