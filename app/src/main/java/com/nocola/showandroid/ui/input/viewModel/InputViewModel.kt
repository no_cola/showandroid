package com.nocola.showandroid.ui.input.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nocola.showandroid.common.Event
import com.nocola.showandroid.domain.ILoadPointsUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

private const val EMPTY_MESSAGE = ""

class InputViewModel @Inject constructor(
    private val loadPointsUseCase: ILoadPointsUseCase
) : ViewModel() {
    private val _disposables = CompositeDisposable()
    private val _loadEvent = PublishSubject.create<Unit>()
    private var _state = BehaviorSubject.create<State>()
    private val _uiState = MutableLiveData<Event<State>>()

    val uiState = _uiState as LiveData<Event<State>>
    private val minAmount = 1
    val maxAmount = 1000
    var amount = 0
        set(value) {
            field = value
            when {
                value < minAmount -> _state.onNext(State.Initial)
                value > maxAmount -> {
                    _state.onNext(State.Wrong)
                }
                else -> _state.onNext(State.Ready)
            }
        }


    init {
        _loadEvent.skipWhile { amount < minAmount }
            .switchMap {
                loadPointsUseCase.loadPoints(amount)
                    .subscribeOn(Schedulers.io())
                    .onErrorReturn {
                        _state.onNext(
                            State.Error(it.message ?: EMPTY_MESSAGE)
                        )
                        emptyList()
                    }
                    .toObservable()
            }
            .doOnNext {
                _state.onNext(State.Loading) }
            .subscribe {
                if (it.isNotEmpty())
                    _state.onNext(State.Loaded)
                else
                    _state.onNext(State.Ready)
            }
            .addTo(_disposables)

        _state
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
            _uiState.value = Event(it)
        }.addTo(_disposables)

        // Start
        _state.onNext(State.Initial)
    }

    fun load() {
        _loadEvent.onNext(Unit)
    }

    override fun onCleared() {
        _disposables.clear()
        super.onCleared()
    }
}

sealed class State {
    object Initial : State()
    object Wrong : State()
    object Ready : State()
    object Loading : State()
    object Loaded : State()
    class Error(val message: String) : State()
}