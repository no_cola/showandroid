package com.nocola.showandroid.ui.graph.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.nocola.showandroid.R
import com.nocola.showandroid.common.BaseFragment
import com.nocola.showandroid.databinding.FragmentGraphBinding
import com.nocola.showandroid.di.IDaggerApp
import com.nocola.showandroid.di.ViewModelFactory
import com.nocola.showandroid.ui.graph.viewModel.GraphViewModel
import javax.inject.Inject

class GraphFragment : BaseFragment() {

    private var binding: FragmentGraphBinding? = null

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private val viewModel: GraphViewModel by viewModels { viewModelFactory }

    override fun onAttach(context: Context) {
        (context.applicationContext as IDaggerApp).inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) : View? {
        binding = FragmentGraphBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            toolbar.title = getString(R.string.graph)
            toolbar.setNavigationOnClickListener {
                findNavController().navigateUp()
            }

            viewModel.table.observe(viewLifecycleOwner) {
                tableView.set(it)
            }

            viewModel.graph.observe(viewLifecycleOwner) {
                graphView.data = it
            }

            viewModel.reload()
        }



    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }
}