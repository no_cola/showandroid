package com.nocola.showandroid.ui.graph.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class TableView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayoutCompat(context, attrs, defStyleAttr) {
    private val recyclerView = RecyclerView(context)
    private val pointsAdapter = PointsAdapter()

    init {
        orientation = VERTICAL
        layoutParams = ViewGroup.LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT
        )

        // Add header
        val row = RowView(context).apply {
            model = arrayOf("X", "Y").map { CellModel(it) }.toTypedArray()
        }
        addView(row)

        // Add table
        recyclerView.apply {
            val ll = LinearLayoutManager(context)
            layoutManager = ll
            adapter = pointsAdapter
            overScrollMode = OVER_SCROLL_NEVER
            layoutParams = ViewGroup.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT
            )
            addItemDecoration(CellDecorator())
        }

        addView(recyclerView)
    }

    fun set(items: Array<RowModel>) {
        pointsAdapter.items = items
    }
}

class RowModel(val data: Array<CellModel>)

private class PointsAdapter : RecyclerView.Adapter<PointViewViewHolder>() {
    var items: Array<RowModel> = arrayOf()
        @SuppressLint("NotifyDataSetChanged")
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PointViewViewHolder {
        return PointViewViewHolder(RowView(parent.context))
    }

    override fun onBindViewHolder(holder: PointViewViewHolder, position: Int) {
        holder.rowView.model = items[position].data
    }

    override fun getItemCount() = items.size

}

private class PointViewViewHolder(view: RowView) :
    RecyclerView.ViewHolder(view) {
    val rowView = itemView as RowView
}

