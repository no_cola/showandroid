package com.nocola.showandroid.ui.graph.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.nocola.showandroid.domain.IGetPointsUseCase
import com.nocola.showandroid.model.repository.Point
import com.nocola.showandroid.ui.graph.view.CellModel
import com.nocola.showandroid.ui.graph.view.RowModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GraphViewModel @Inject constructor(
    private val getPointsUseCase: IGetPointsUseCase
) :
    ViewModel() {
    private val _disposable = CompositeDisposable()
    private val _table = MutableLiveData<Array<RowModel>>()
    private val _graph = MutableLiveData<Array<Point>>()
    val table = _table as LiveData<Array<RowModel>>
    val graph = _graph as LiveData<Array<Point>>

    fun reload() {
        getPointsUseCase.getPoints()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                _graph.value = it.toTypedArray()
                val uiModels: Array<RowModel> = it.map { point ->
                    RowModel(
                        arrayOf(
                            CellModel(point.x.toString()),
                            CellModel(point.y.toString())
                        )
                    )
                }.toTypedArray()
                _table.value = uiModels
            }, {}).addTo(_disposable)
    }

    override fun onCleared() {
        _disposable.clear()
        super.onCleared()
    }
}