package com.nocola.showandroid.model.repository

import io.reactivex.Single

/**
 * Abstraction that is used as a source of [Point]s
 */
interface IPointsRepository {
    /**
     * Load exact amount of point.
     */
    fun load(amount: Int) : Single<List<Point>>

    /**
     * Get persisted [Point]s
     */
    fun getGraph() : Single<List<Point>>
}