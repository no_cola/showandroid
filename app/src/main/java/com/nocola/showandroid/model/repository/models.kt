package com.nocola.showandroid.model.repository

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Point(val x: Float, val y: Float): Parcelable

@Parcelize
data class PointsResponse(val points: List<Point>): Parcelable