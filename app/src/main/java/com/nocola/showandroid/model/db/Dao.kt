package com.nocola.showandroid.model.db

import androidx.room.*
import io.reactivex.Single

/**
 * 'Data Access Object' to Room database when stores [PointDataBaseModel]
 */
@Dao
interface PointDao {
    @Query("SELECT * FROM $TABLE_NAME ORDER BY x ASC")
    fun getAllPoints() : Single<List<PointDataBaseModel>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(pointDataBase: PointDataBaseModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(pointDataBases: List<PointDataBaseModel>)


    @Query("DELETE FROM $TABLE_NAME")
    fun deleteAll()
}