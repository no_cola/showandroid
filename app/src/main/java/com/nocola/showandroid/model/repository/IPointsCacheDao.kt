package com.nocola.showandroid.model.repository

import io.reactivex.Single

/**
 * Abstraction for 'Data Access Object' that is used to work as persistent cache
 * of [Point]s
 */
interface IPointsCacheDao {
    /**
     * Get persisted [Point]s
     */
    fun getPoints() : Single<List<Point>>

    /**
     * Persist loaded [Point]s
     */
    fun save(points: List<Point>)
}