package com.nocola.showandroid.model.repository

import com.nocola.showandroid.model.db.PointDao
import com.nocola.showandroid.model.db.PointDataBaseModel
import io.reactivex.Single

/**
 * Concrete implementation of "Data Access Object' that is backed
 * with Room database.
 */
class PointsDataBaseCacheDao(private val db: PointDao) :
    IPointsCacheDao {
    override fun getPoints(): Single<List<Point>> {
        return db.getAllPoints()
            .map { p -> p.map { Point(it.x, it.y) } }
    }

    override fun save(points: List<Point>) {
        db.deleteAll()
        db.insertAll(points.map { PointDataBaseModel(0, it.x, it.y) })
    }
}