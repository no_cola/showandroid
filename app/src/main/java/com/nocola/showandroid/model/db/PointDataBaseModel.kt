package com.nocola.showandroid.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey

const val TABLE_NAME = "point_table"

@Entity(tableName = TABLE_NAME)
data class PointDataBaseModel(
    @PrimaryKey(autoGenerate = true) val id: Int,
    val x: Float,
    val y: Float
)