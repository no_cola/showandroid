package com.nocola.showandroid.model.repository

import io.reactivex.Single

class NetworkPointsDao(private val api: PointsApi) : IPointsDao {
    override fun load(amount: Int): Single<List<Point>> {
        return api.getPoints(amount).map { it.points }
    }

}