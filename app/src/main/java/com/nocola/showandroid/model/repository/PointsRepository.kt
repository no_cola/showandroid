package com.nocola.showandroid.model.repository

import io.reactivex.Single

/**
 * Abstraction for source of [Point]s. Represents 'model' layer.
 */
class PointsRepository(
    private val dao: IPointsDao,
    private val pointsCacheDao: IPointsCacheDao
) :
    IPointsRepository {
    override fun load(amount: Int): Single<List<Point>> {
        return dao.load(amount).doOnSuccess { pointsCacheDao.save(it) }
    }

    override fun getGraph(): Single<List<Point>> {
        return pointsCacheDao.getPoints()
    }
}