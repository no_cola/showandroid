package com.nocola.showandroid.model.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

/**
 * Room database. Stores [PointDataBaseModel] table.
 */

private const val POINT_DB = "point_database"

@Database(entities = [PointDataBaseModel::class], version = 1, exportSchema = false)
abstract class PointsDatagBase : RoomDatabase() {

    abstract fun pointDao(): PointDao

    // Keep database as singleton.
    companion object {
        @Volatile
        private var instance: PointsDatagBase? = null

        fun getDatabase(context: Context): PointsDatagBase {
            return instance ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PointsDatagBase::class.java,
                    POINT_DB
                ).build()
                this.instance = instance
                instance
            }
        }
    }
}