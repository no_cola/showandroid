package com.nocola.showandroid.model.repository

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface PointsApi {
    @GET("/api/test/points")
    fun getPoints(
        @Query("count") count: Int
    ): Single<PointsResponse>
}