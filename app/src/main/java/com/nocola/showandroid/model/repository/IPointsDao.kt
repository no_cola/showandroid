package com.nocola.showandroid.model.repository

import io.reactivex.Single

/**
 * Abstraction for 'Data Access Object' that is used to work as source of
 * [Point]s
 */
interface IPointsDao {
    fun load(amount: Int): Single<List<Point>>
}