package com.nocola.showandroid.common

import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import com.nocola.showandroid.R
import io.reactivex.disposables.CompositeDisposable

open class BaseFragment : Fragment() {
    protected val disposable = CompositeDisposable()

    override fun onDestroyView() {
        disposable.clear()
        super.onDestroyView()
    }
}

fun defaultAnimations() : NavOptions {
    return NavOptions
        .Builder()
        .setEnterAnim(R.anim.slide_in)
        .setPopEnterAnim(R.anim.nav_default_pop_enter_anim)
        .setPopExitAnim(R.anim.slide_out)
        .build()
}