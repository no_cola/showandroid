package com.nocola.showandroid.common

import android.app.Activity
import android.util.TypedValue
import androidx.core.content.ContextCompat

/**
 * @param ctx Context with theme, must be Activity.
 * @return Resource id for attribute.
 */
fun resolveAttribute(ctx: Activity, attr: Int) : Int {
    val value = TypedValue()
    ctx.theme.resolveAttribute(attr, value, false)
    return value.data
}

/**
 * Load color from indirect resource id.
 */
fun color(ctx: Activity, attr: Int) : Int {
    return ContextCompat.getColor(ctx, resolveAttribute(ctx, attr))
}