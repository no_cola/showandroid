package com.nocola.showandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.nocola.showandroid.di.IDaggerApp
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController

    private fun initNavController() {
        val fragment =
            (supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment)
        navController = fragment.navController
        navController.setGraph(R.navigation.main_navigation)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        (applicationContext as IDaggerApp).inject(this)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        initNavController()
    }
}