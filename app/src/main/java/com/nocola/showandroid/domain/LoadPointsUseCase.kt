package com.nocola.showandroid.domain

import com.nocola.showandroid.model.repository.IPointsRepository
import com.nocola.showandroid.model.repository.Point
import io.reactivex.Single

class LoadPointsUseCase(private val repository: IPointsRepository) :
    ILoadPointsUseCase {
    override fun loadPoints(amount: Int): Single<List<Point>> {
        return repository.load(amount)
    }
}