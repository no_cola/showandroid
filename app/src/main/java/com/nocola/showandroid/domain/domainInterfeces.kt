package com.nocola.showandroid.domain

import com.nocola.showandroid.model.repository.Point
import io.reactivex.Single

/**
 * Retrieve all points.
 */
interface IGetPointsUseCase {
    fun getPoints() : Single<List<Point>>
}

/**
 * Load point exact amount of points.
 */
interface ILoadPointsUseCase {
    fun loadPoints(amount: Int) : Single<List<Point>>
}