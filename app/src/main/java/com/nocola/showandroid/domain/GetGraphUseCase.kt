package com.nocola.showandroid.domain

import com.nocola.showandroid.model.repository.IPointsRepository
import com.nocola.showandroid.model.repository.Point
import io.reactivex.Single

class GetPointsUseCase(private val repository: IPointsRepository) :
    IGetPointsUseCase {
    override fun getPoints(): Single<List<Point>> {
        return repository.getGraph()
    }
}