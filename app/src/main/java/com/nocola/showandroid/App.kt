package com.nocola.showandroid

import android.app.Application
import com.nocola.showandroid.di.*
import com.nocola.showandroid.ui.graph.view.GraphFragment
import com.nocola.showandroid.ui.input.view.InputFragment


class App : Application(), IDaggerApp {

    private fun initComponent() {
        component = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .apiModule(ApiModule(BASE_URL))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        initComponent()
    }

    override fun inject(obj: Any) {
        when (obj) {
            is MainActivity -> component.inject(obj)
            is InputFragment -> component.inject(obj)
            is GraphFragment -> component.inject(obj)
        }
    }


    companion object {
        lateinit var component: AppComponent
    }
}
