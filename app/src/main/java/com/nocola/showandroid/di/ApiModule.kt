package com.nocola.showandroid.di

import android.content.Context
import com.google.gson.Gson
import com.nocola.showandroid.BuildConfig
import com.nocola.showandroid.model.repository.PointsApi
import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

private const val OK_HTTP_CLIENT = "ok_http_client"
private const val API_RETROFIT = "api_retrofit"

const val BASE_URL = "https://hr-challenge.interactivestandard.com"

@Module
class ApiModule(private var baseUrl: String) {

    @Provides
    @Singleton
    @Named(OK_HTTP_CLIENT)
    fun providesOkhttpClient(context: Context): OkHttpClient {
        val client = OkHttpClient.Builder()
            .addNetworkInterceptor(
                HttpLoggingInterceptor().setLevel(
                    if (BuildConfig.DEBUG)
                        HttpLoggingInterceptor.Level.BODY
                    else
                        HttpLoggingInterceptor.Level.BASIC
                )
            )
            .addInterceptor(ChuckInterceptor(context))
        return client.build()
    }

    @Provides
    @Named(API_RETROFIT)
    fun providesRetrofit(
        @Named(OK_HTTP_CLIENT) okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(Gson())).build()
    }

    @Provides
    fun graphApi(@Named(API_RETROFIT) retrofit: Retrofit): PointsApi {
        return retrofit.create(PointsApi::class.java)
    }
}