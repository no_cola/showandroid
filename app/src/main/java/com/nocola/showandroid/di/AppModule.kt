package com.nocola.showandroid.di

import android.app.Application
import android.content.Context
import com.nocola.showandroid.domain.GetPointsUseCase
import com.nocola.showandroid.model.db.PointsDatagBase
import com.nocola.showandroid.domain.LoadPointsUseCase
import com.nocola.showandroid.domain.IGetPointsUseCase
import com.nocola.showandroid.domain.ILoadPointsUseCase
import com.nocola.showandroid.model.repository.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class AppModule(private val app: Application) {

    @Singleton
    @Provides
    fun providesAppContext(): Context {
        return app
    }

    @Singleton
    @Provides
    fun providesApp(): Application {
        return app
    }

    @Singleton
    @Provides
    fun providesLoadPointsUseCase(repo: IPointsRepository): ILoadPointsUseCase {
        return LoadPointsUseCase(repo)
    }

    @Singleton
    @Provides
    fun providesGetPointsUseCase(repo: IPointsRepository): IGetPointsUseCase {
        return GetPointsUseCase(repo)
    }

    @Singleton
    @Provides
    fun providesPointsRepository(
        dao: IPointsDao,
        pointsCacheDao: IPointsCacheDao
    ): IPointsRepository {
        return PointsRepository(dao, pointsCacheDao)
    }

    @Provides
    @Singleton
    fun providesGraphDao(pointsApi: PointsApi): IPointsDao {
        return NetworkPointsDao(pointsApi)
    }

    @Provides
    @Singleton
    fun providesCachePointsDao(appCtx: Application) : IPointsCacheDao {
        return PointsDataBaseCacheDao(PointsDatagBase.getDatabase(appCtx).pointDao())
    }
}