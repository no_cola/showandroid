package com.nocola.showandroid.di

interface IDaggerApp {
    fun inject(obj: Any)
}
