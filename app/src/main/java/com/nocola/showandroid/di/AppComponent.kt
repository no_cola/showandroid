package com.nocola.showandroid.di

import com.nocola.showandroid.MainActivity
import com.nocola.showandroid.ui.graph.view.GraphFragment
import com.nocola.showandroid.ui.input.view.InputFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        (AppModule::class),
        (ViewModelsModule::class),
        (ApiModule::class)
    ]
)
interface AppComponent {
    fun inject(obj: MainActivity)
    fun inject(obj: InputFragment)
    fun inject(obj: GraphFragment)
}