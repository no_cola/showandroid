package com.nocola.showandroid.di

import androidx.lifecycle.ViewModel
import com.nocola.showandroid.ui.graph.viewModel.GraphViewModel
import com.nocola.showandroid.ui.input.viewModel.InputViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelsModule {
    @Binds
    @IntoMap
    @ViewModelKey(value = InputViewModel::class)
    abstract fun bindInputViewModel(viewModel: InputViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(value = GraphViewModel::class)
    abstract fun bindGraphViewModel(viewModel: GraphViewModel): ViewModel

}