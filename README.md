Simple test project ShowAndroid
===============================

Abilities
---------
- Load points from network.
- Keep points to persistence.
- Handle portrait and landscape.
- Expose points in table and graphic.
- Graphic is scalable.

Architecture
------------

Project divided into main 3 layers
- UI (View, ViewModel)
- Domain (Use cases)
- Model (Repositories)

Main libraries
---------------

Retrofit, Dagger2, RxKotlin, Room database, Android Architecture
Components (LiveData, ViewModel, Navigation).

Data flow
=========

From UI layer of the first screen command triggers repository to load data from
network. After data is retrieved it saved to database.

Database used as a persistent cache, to keep data safe in case of activity
death (rotation, system kill).

Second screen shows retrieved on previous screen. Data supplied by repository
backed by Room database.

Тhis architecture allows us to securely hold data in situations of screen flip
and complete re-creation of activities.

In addition GraphView saves/restores internal state to/from bundle when screen rotates.
GraphView enables setting parameters from xml.

Network
-------

- 'Retrofit' is used to facilitate REST calls.
- 'Chuck' is used to facilitate debugging network calls.


UI
--

TableView  - implemented as a compound view (without xml).
GraphView - implemented as custom view with overriding 'onDraw'. Keeps its state
    preventing clearing in case of recreating activity. Can be scaled, can be
    configured from xml.

Themes, styles, colors
- All view settings are placed to styles and themes.
- Colors are kept and referenced only as 'indirect' resource ids (?attr/). This
approach leaves us ability dynamically change themes of the whole app.

Tests
-----

'Model' layer and 'domain' covered by unit tests.